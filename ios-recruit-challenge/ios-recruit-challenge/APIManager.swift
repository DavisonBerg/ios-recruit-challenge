//
//  APIManager.swift
//  ios-recruit-challenge
//
//  Created by Davison Dantas on 08/02/2018.
//  Copyright © 2018 Davison. All rights reserved.
//
import UIKit
import Foundation
import Alamofire
import PromiseKit

class APIManager{
    
    var totalpages = 3
    static let shared = APIManager()
    public var errorCode: Int?
    public var page: Int!
    public var lastPage: Int!
    private init() {
       
        
    }

    func fetchItemsFromApi(pageNumber: Int) -> Promise<[Movie]>{
        return Promise<[Movie]>{
            fullfil,reject -> Void in
                let url = URL(string: "https://api.themoviedb.org/3/movie/popular?api_key=\(apiKey)&page=\(pageNumber)")
                Alamofire.request(url!).validate().responseString { response in
               // print(url)
                switch(response.result){
                case .success(let responseString):
                    //print(responseString)
                    let itemResponse =  MoviesApiResponse(JSONString:"\(responseString)")
                    self.page = itemResponse?.page
                    self.lastPage = itemResponse?.total_pages

                    fullfil((itemResponse?.movies)!)
                        
                case .failure(let error):
                    
                    if let code = response.response?.statusCode{
                        self.errorCode = code
                        reject(error)
                    }
                    
                }
            }
            
        }
    }
    
    func fetchItemsFromApiGenre() -> Promise<[Genre]>{
        
        return Promise<[Genre]>{
            fullfil,reject -> Void in
            return Alamofire.request(genreURL!).validate().responseString{ (response) in
                switch(response.result){
                case .success(let responseString):
                
                    let itemResponse = GenreApiResponse(JSONString:"\(responseString)")
                    fullfil((itemResponse?.genres)!)
                    
                case .failure(let error):
                    if let code = response.response?.statusCode{
                        self.errorCode = code
                        reject(error)
                    }
                    
                }
                
            }
        }
    }
    
}
    

    

    

