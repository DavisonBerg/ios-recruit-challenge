//
//  APIStatusCode.swift
//  ios-recruit-challenge
//
//  Created by Davison Dantas on 11/02/2018.
//  Copyright © 2018 Davison. All rights reserved.
//

import UIKit
import ObjectMapper

class APIStatusCode: Mappable {
    
    var status: Int?
    var status_message: String?
    var success: Bool?

    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        status <- map["status_code"]
        status_message <- map["status_message"]
        success <- map["success"]
       
    }
}
