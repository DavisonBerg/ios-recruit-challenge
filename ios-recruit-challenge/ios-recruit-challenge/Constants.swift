
//
//  Constants.swift
//  ios-recruit-challenge
//
//  Created by Davison Dantas on 08/02/2018.
//  Copyright © 2018 Davison. All rights reserved.
//

import Foundation

let apiKey = "78552816332d5534f9214ff40184d623"
let page = 0
let url = URL(string: "https://api.themoviedb.org/3/movie/popular?api_key=\(apiKey)&page=\(page)")
let baseImageURL = "https://image.tmdb.org/t/p/w500"
let genreURL = URL(string: "https://api.themoviedb.org/3/genre/movie/list?api_key=\(apiKey)")

