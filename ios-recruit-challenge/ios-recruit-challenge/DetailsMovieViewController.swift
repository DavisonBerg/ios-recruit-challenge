//
//  DetailsMovieViewController.swift
//  ios-recruit-challenge
//
//  Created by Davison Dantas on 08/02/2018.
//  Copyright © 2018 Davison. All rights reserved.
//

import UIKit
import CoreData
import Foundation
import ObjectMapper
import AlamofireImage

class DetailsMovieViewController: UIViewController {

    var context: NSManagedObjectContext!
    
    var movieDetail: Movie!
    var nameDetail = ""
    var imageUrlDetail = ""
    var descriptionDetail = ""
    var dateDetail = ""
    var genreDetail = ""
    var dateFormatter = DateFormatter()

    @IBOutlet weak var favButton: UIButton!
    
    @IBOutlet weak var movieImage: UIImageView!
    @IBOutlet weak var movieDescription: UILabel!
    @IBOutlet weak var movieGenre: UILabel!
    @IBOutlet weak var movieYear: UILabel!
    @IBOutlet weak var movieTitle: UILabel!
    
    var contextDetails: NSManagedObjectContext!
    var datafavorites: [NSManagedObject] = []
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        contextDetails = appDelegate.persistentContainer.viewContext
        
        dateFormatter.dateFormat = "yyyy-MM-dd"
        if let date = dateFormatter.date(from: dateDetail){
            
            dateFormatter.dateFormat = "yyyy"
            let convertDate = dateFormatter.string(from: date)
            movieYear.text = convertDate
         
        }
        
        movieGenre.text = genreDetail
        movieTitle.text = nameDetail
        movieDescription.text = descriptionDetail
        let movieImageURL = URL(string: baseImageURL + imageUrlDetail)
        movieImage.af_setImage(withURL: movieImageURL!)
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.recoverFavorites()
    }

    
    @IBAction func favoriteButton(_ sender: UIButton) {
        self.saveFavorite()
        self.navigationController?.popToRootViewController(animated: true)     
        
    }
    
    func recoverFavorites(){
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "MovieFavoriteEntity")
  
        do {
            let favoritesRecovered = try contextDetails.fetch(request)
            self.datafavorites = favoritesRecovered as! [NSManagedObject]
            for data in self.datafavorites {
                if data.value(forKey:"title") as? String == nameDetail{
                    favButton.setImage(UIImage(named:"favorite_full_icon.png"), for: .normal)
                } else{
                    favButton.setImage(UIImage(named:"favorite_empty_icon.png"), for: .normal)
                }
                
            }
            view.reloadInputViews()
            
        } catch let err {
            print("Erro ao carregar favorito: "+err.localizedDescription)
        }
        
    }
    
    func saveFavorite() {
        
        //cria objeto para novo favorito
        let newfavorite = NSEntityDescription.insertNewObject(forEntityName: "MovieFavoriteEntity", into: contextDetails)
        
        //configura favorito
        newfavorite.setValue(movieDetail.name!, forKey: "title")
        newfavorite.setValue(movieDetail.imageURL!, forKey: "imageURL")
        newfavorite.setValue(movieDetail.description!, forKey: "descriptionMovie")
        newfavorite.setValue(movieDetail.date!, forKey: "date")
        
        //salvar os  dados
        do {
            try contextDetails.save()
            print("Sucesso ao salvar favorito")
        } catch let err {
            print("Erro ao salvar favorito: "+err.localizedDescription)
        }
        
    }
}
