//
//  FavoriteTableViewCell.swift
//  ios-recruit-challenge
//
//  Created by Davison Dantas on 09/02/2018.
//  Copyright © 2018 Davison. All rights reserved.
//

import UIKit

class FavoriteTableViewCell: UITableViewCell {

    @IBOutlet weak var movieImageFavorite: UIImageView!
    @IBOutlet weak var movieTitleFavorite: UILabel!
    @IBOutlet weak var movieDescriptionFavorite: UILabel!
    @IBOutlet weak var movieYearFavorite: UILabel!

}
