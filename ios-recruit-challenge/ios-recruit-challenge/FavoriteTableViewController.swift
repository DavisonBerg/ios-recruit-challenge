//
//  FavoriteTableViewController.swift
//  ios-recruit-challenge
//
//  Created by Davison Dantas on 09/02/2018.
//  Copyright © 2018 Davison. All rights reserved.
//

import UIKit
import CoreData
import Foundation
import ObjectMapper
import AlamofireImage

class FavoriteTableViewController: UITableViewController {

    var context: NSManagedObjectContext!
    var datafavorites: [NSManagedObject] = []
    
    var dataSourceArrayFavorite = [Movie]()
    var movieFavorite: Movie!
    var dateFormatterFavorite = DateFormatter()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //tableView.estimatedRowHeight = 130
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        context = appDelegate.persistentContainer.viewContext
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.recoverFavorites()
    }
    
    func recoverFavorites(){
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "MovieFavoriteEntity")
    
        
        do {
            let favoritesRecovered = try context.fetch(request)
            self.datafavorites = favoritesRecovered as! [NSManagedObject]
            self.tableView.reloadData()
        } catch let err {
            print("Erro ao carregar favorito: "+err.localizedDescription)
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {

        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return self.datafavorites.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "favoriteCustomCell", for: indexPath) as! FavoriteTableViewCell
        
        let favorite = self.datafavorites[indexPath.row]
        
        cell.movieTitleFavorite.text = favorite.value(forKey: "title") as? String
        cell.movieDescriptionFavorite.text = favorite.value(forKey: "descriptionMovie") as? String
        
        let poster_path = favorite.value(forKey: "imageURL") as? String
        let movieImageURL = URL(string: baseImageURL + poster_path!)
        cell.movieImageFavorite.af_setImage(withURL: movieImageURL!)
        
        dateFormatterFavorite.dateFormat = "yyyy-MM-dd"
        if let dateFavorite = dateFormatterFavorite.date(from: (favorite.value(forKey: "date") as? String)!){
            dateFormatterFavorite.dateFormat = "yyyy"
            let convertDateFavorite = dateFormatterFavorite.string(from: dateFavorite)
            cell.movieYearFavorite.text = convertDateFavorite
        }
        
        return cell
        
    }
    
 
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return true
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let unfavorite = UITableViewRowAction(style: .normal, title: "Unfavorite") { (action, index) in
            //print("unfavorite button tapped")
            
            let unfavoriteMovie = self.datafavorites[indexPath.row]
            
            self.context.delete(unfavoriteMovie)
            self.datafavorites.remove(at: indexPath.row)
         
            
            self.tableView.deleteRows(at: [indexPath], with: .automatic)
            
            do {
                try self.context.save()
            } catch let err {
                print("Erro ao remover favorito: "+err.localizedDescription)
            }
        }
        
        unfavorite.backgroundColor = .red
        
        return [unfavorite]
    }

}
