//
//  Genre.swift
//  ios-recruit-challenge
//
//  Created by Davison Dantas on 09/02/2018.
//  Copyright © 2018 Davison. All rights reserved.
//

import UIKit
import ObjectMapper

class Genre: Mappable {
    
    var idGenre: Int?
    var nameGenre: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        idGenre <- map["id"]
        nameGenre <- map["name"]
 
    }
}
