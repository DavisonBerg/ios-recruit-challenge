//
//  GenreApiResponse.swift
//  ios-recruit-challenge
//
//  Created by Davison Dantas on 09/02/2018.
//  Copyright © 2018 Davison. All rights reserved.
//

import Foundation
import ObjectMapper

class GenreApiResponse: Mappable{
    
    var genres : [Genre]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        genres <- map ["genres"]
    }
    
}
