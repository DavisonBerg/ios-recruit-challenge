//
//  Movie.swift
//  ios-recruit-challenge
//
//  Created by Davison Dantas on 08/02/2018.
//  Copyright © 2018 Davison. All rights reserved.
//

import UIKit
import ObjectMapper

class Movie: Mappable {
    
    var name: String?
    var imageURL: String?
    var description: String?
    var date: String?
    var genreArray: [Int] = []
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        name <- map["title"]
        imageURL <- map["poster_path"]
        description <- map["overview"]
        date <- map["release_date"]
        genreArray <- map["genre_ids"]
    }
}

