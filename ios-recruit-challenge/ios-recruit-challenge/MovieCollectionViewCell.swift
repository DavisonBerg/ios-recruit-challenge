//
//  MovieCollectionViewCell.swift
//  ios-recruit-challenge
//
//  Created by Davison Dantas on 08/02/2018.
//  Copyright © 2018 Davison. All rights reserved.
//

import UIKit

class MovieCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var favoriteImage: UIImageView!
    @IBOutlet weak var movieName: UILabel!
    @IBOutlet weak var movieImage: UIImageView!
}
