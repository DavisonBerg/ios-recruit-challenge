//
//  MovieApiResponse.swift
//  ios-recruit-challenge
//
//  Created by Davison Dantas on 08/02/2018.
//  Copyright © 2018 Davison. All rights reserved.
//

import Foundation
import ObjectMapper

class MoviesApiResponse: Mappable{
    
    var movies : [Movie]?
    var page: Int?
    var total_results: Int?
    var total_pages: Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        movies <- map ["results"]
        page <- map["page"]
        total_results <- map["total_results"]
        total_pages <- map["total_pages"]
    }
    
}
