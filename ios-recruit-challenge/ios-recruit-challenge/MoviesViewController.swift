//
//  ViewController.swift
//  ios-recruit-challenge
//
//  Created by Davison Dantas on 08/02/2018.
//  Copyright © 2018 Davison. All rights reserved.
//

import UIKit
import Foundation
import CoreData
import ObjectMapper
import AlamofireImage

class MoviesViewController: UICollectionViewController, UISearchBarDelegate{


    //Searching
    //var filtered = [Movie]()
    var searchActive: Bool = false
    let searchController = UISearchController(searchResultsController: nil)
    
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    var dataSourceArray = [Movie]()
    var dataSourceArrayGenre = [Genre]()
    
 
    
    var errorInt: Int?
    var numberOfItensPerSection = 20
    var page: Int!
    var lastPage: Int!
    var pageNumberController = 1
    let requestManager = APIManager.shared
    
    @IBOutlet weak var imageError: UIImageView!
    
    
    var context: NSManagedObjectContext!
    var datafavorites: [NSManagedObject] = []
    
    override var prefersStatusBarHidden: Bool{
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.recoverFavorites()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        context = appDelegate.persistentContainer.viewContext
        
      //Searching

        
        self.searchBarSetup()
        
      //Loading
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        view.addSubview(activityIndicator)
        
        activityIndicator.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()
     
        self.paginationAPI(pageNumber: pageNumberController)
      
       
    }
    
    func recoverFavorites(){
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "MovieFavoriteEntity")
        
        do {
            let favoritesRecovered = try context.fetch(request)
            self.datafavorites = favoritesRecovered as! [NSManagedObject]
//            for data in self.datafavorites {
//                if data.value(forKey:"title") as? String == nameDetail{
//                    favButton.setImage(UIImage(named:"favorite_full_icon.png"), for: .normal)
//                } else{
//                    favButton.setImage(UIImage(named:"favorite_empty_icon.png"), for: .normal)
//                }
//
//            }
            view.reloadInputViews()
            
        } catch let err {
            print("Erro ao carregar favorito: "+err.localizedDescription)
        }
        
    }
    
    func searchBarSetup() {
        let searchBar = UISearchBar(frame: CGRect(x:0, y:0, width:(UIScreen.main.bounds.width), height:70))
        
        searchBar.delegate = self
        
        self.navigationItem.titleView = searchBar
       // view.addSubview(searchBar)
    }
   
    //MARK: SearchBar Delegate
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText.isEmpty{
            self.paginationAPI(pageNumber: 1)
            self.collectionView?.reloadData()
        }
        filterCollectionView(text: searchText)
    }
    
    func filterCollectionView(text:String){
        dataSourceArray = dataSourceArray.filter { (item) -> Bool in
            return (item.name?.contains(text))!
        }
        self.collectionView?.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
 
            return dataSourceArray.count    //return number of rows in section

    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "customCell", for: indexPath) as! MovieCollectionViewCell

            cell.movieName.text = dataSourceArray[indexPath.row].name
            
            let poster_path = dataSourceArray[indexPath.row].imageURL
            let movieImageURL = URL(string: baseImageURL + poster_path!)
            cell.movieImage.af_setImage(withURL: movieImageURL!)

        for data in self.datafavorites {
            if data.value(forKey:"title") as? String == dataSourceArray[indexPath.row].name{
               cell.favoriteImage.image = UIImage(named:"favorite_full_icon.png")
            } 
            
        }
        
        if indexPath.row == dataSourceArray.count - 1{
            // get next page
            self.lastPage = APIManager.shared.lastPage
            self.page = APIManager.shared.page

            pageNumberController += 1
            self.paginationAPI(pageNumber: pageNumberController)
        }
        
        return cell
    }

    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let mainStoryBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let destVC = mainStoryBoard.instantiateViewController(withIdentifier: "DetailsMovieViewController") as! DetailsMovieViewController
        var genre = ""
        for idListMovie in dataSourceArray[indexPath.row].genreArray {
            for idList in dataSourceArrayGenre{
                if (idListMovie == idList.idGenre!){

                    genre = idList.nameGenre!+", "+genre
      
                }
            }
        }

        destVC.genreDetail = genre
        destVC.nameDetail = dataSourceArray[indexPath.row].name!
        destVC.imageUrlDetail = dataSourceArray[indexPath.row].imageURL!
        destVC.descriptionDetail = dataSourceArray[indexPath.row].description!
        destVC.dateDetail = dataSourceArray[indexPath.row].date!
        
        destVC.movieDetail = dataSourceArray[indexPath.row]
        
        destVC.navigationItem.title = "Movie"
        self.navigationController?.pushViewController(destVC, animated: true)
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let offsetY = scrollView.contentOffset.y
        let contenHeight = scrollView.contentSize.height
        
        if offsetY > contenHeight - scrollView.frame.size.height{
            numberOfItensPerSection += 6
            self.collectionView?.reloadData()
            //scrollView.isUserInteractionEnabled = false
        }
        
        scrollView.keyboardDismissMode = .onDrag
        
    }
    
    func paginationAPI(pageNumber: Int){
        let apiCall = APIManager.shared.fetchItemsFromApi(pageNumber: pageNumber)
        apiCall.then{
            movies -> Void in
            
            self.dataSourceArray += movies
            self.collectionView?.reloadData()
            self.activityIndicator.stopAnimating()
            UIApplication.shared.endIgnoringInteractionEvents()
            //self.imageError.isHidden = true
            let apiCallGenre = APIManager.shared.fetchItemsFromApiGenre()
            apiCallGenre.then{
                genres -> Void in
                self.dataSourceArrayGenre = genres
                }.catch{error -> Void in
                    
                    // print(error)
            }
            
            }.catch{error -> Void in
                self.activityIndicator.stopAnimating()
                UIApplication.shared.endIgnoringInteractionEvents()
                self.errorInt = APIManager.shared.errorCode
                self.imageError.isHidden = false
                print(self.errorInt!)
                if self.errorInt == 400{
                    self.imageError.image = UIImage(named: "Empty")
                }else{
                    self.imageError.image = UIImage(named: "error")
                }
        }
    }
}

